package net.huadong.nio;

public class NioTimeClient {
    public static void main(String[] args) {
        int port = 8080;
        String ip = "127.0.0.1";

        new Thread(new TimeClientHandle(ip, port), "NIO-TIMECLIENT-001").start();
    }
}
