package net.huadong.nio;

/**
 * 非阻塞IO  NIO 服务端
 * ws
 */
public class NioTimeServer {
    public static void main(String[] args) {
        int port = 8080;

        //创建NIO线程
        MultiplexerTimeServer multiplexerTimeServer = new MultiplexerTimeServer(port);

        //启动线程
        new Thread(multiplexerTimeServer, "NIO-MultiplexerTimeServer-001").start();
    }
}
