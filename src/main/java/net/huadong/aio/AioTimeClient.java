package net.huadong.aio;

/**
 * 异步非阻塞IO 客户端
 * <p>
 * ws
 */
public class AioTimeClient {
    public static void main(String[] args) {
        int port = 8080;
        String ip = "127.0.0.1";
        AsyncTimeClientHandler asyncTimeClientHandler = new AsyncTimeClientHandler(ip, port);
        new Thread(asyncTimeClientHandler, "AIO-Client-001").start();
    }
}
