package net.huadong.aio;

/**
 * 异步非阻塞IO 服务端
 */
public class AioTimeServer {
    public static void main(String[] args) {
        int port = 8080;

        AsyncTimeServerHandler asyncTimeServerHandler = new AsyncTimeServerHandler(port);
        new Thread(asyncTimeServerHandler, "AIO-Server-001").start();
    }
}
