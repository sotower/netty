package net.huadong.vio;

import net.huadong.bio.BioTimeServerHandler;

import java.net.ServerSocket;
import java.net.Socket;

/**
 * 伪NIO服务端  请求服务时间
 * ws
 */
public class VioTimeServer {
    public static void main(String[] args) throws Exception {
        int port = 8080;
        ServerSocket server = null;
        try {
            server = new ServerSocket(port);
            System.out.println("服务启动，端口为" + port);

            /**
             * 与BIO不同的地方，采用线程池的方式优化服务端数量
             * 所以称为伪NIO，并没有在传输改进，而是在线程管理上进行了优化
             */

            TimeServerHandlerExecutePool timeServerHandlerExecutePool = new TimeServerHandlerExecutePool(50, 10000);
            Socket socket = null;
            while (true) {
                socket = server.accept();
                timeServerHandlerExecutePool.execute(new BioTimeServerHandler(socket));
            }

        } finally {
            if (server != null) {
                System.out.println("服务关闭");
                server.close();
            }
        }

    }
}
