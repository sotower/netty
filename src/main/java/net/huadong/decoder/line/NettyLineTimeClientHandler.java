package net.huadong.decoder.line;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.logging.Logger;

/**
 * netty 客户端消息处理
 */
public class NettyLineTimeClientHandler extends ChannelInboundHandlerAdapter {
    private static final Logger logger = Logger.getLogger(NettyLineTimeClientHandler.class.getName());
    private  byte[] req;
    private int count;
    public NettyLineTimeClientHandler() {
        req = ("QUERY_TIME_ORDER"+System.getProperty("line.separator")).getBytes();
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {

        ByteBuf message = null;
        for(int i=0 ;i<100; i++){
            message = Unpooled.buffer(req.length);
            message.writeBytes(req);
            ctx.writeAndFlush(message);
        }

    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        //使用字符串解码器
        String body = (String)msg;
        System.out.println("客户端接收到信息：" + body +"    计数："+ ++count);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.warning("释放资源：" + cause.getMessage());
        ctx.close();
    }
}
