package net.huadong.bio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * 同步阻塞IO  BIO  客户端  请求服务端时间
 * ws
 */
public class BioTimeClient {
    public static void main(String[] args) {
        int port = 8080;
        String ip = "127.0.0.1";
        Socket socket = null;
        BufferedReader in = null;
        PrintWriter out = null;
        try {
            //请求连接服务
            socket = new Socket(ip, port);
            //初始化输入流
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            //初始化输出流
            out = new PrintWriter(socket.getOutputStream(), true);
            //发送指令
            out.println("QUERY_TIME_ORDER");
            System.out.println("客户端发送指令到服务端");

            //等待接受  这里就是阻塞
            String resp = in.readLine();
            System.out.println("服务端返回结果：" + resp);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            if (out != null) {
                out.close();
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}
