package net.huadong.bio;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 同步阻塞IO  BIO  服务端  请求服务端时间
 * ws
 */
public class BioTimeServer {
    public static void main(String[] args) throws IOException {
        int port = 8080;
        ServerSocket server = null;
        try {
            server = new ServerSocket(port);
            System.out.println("服务启动，端口为" + port);
            Socket socket = null;
            //轮询监听socket请求
            while (true) {
                //接收到请求
                socket = server.accept();
                //创建线程处理连接
                new Thread(new BioTimeServerHandler(socket)).start();
            }

        } finally {
            if (server != null) {
                System.out.println("服务关闭");
                server.close();
            }
        }

    }
}
